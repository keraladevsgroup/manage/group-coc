# Group general rules 

- Malayalam
- English

## ഗ്രിപ്പിൽ പാളിക്കേണ്ട  നിയമങ്ങൾ  
- 🕹 സപാമുകള്‍ കര്‍ശനമായി നിരോധിച്ചിരിക്കുന്നു.

- ഫോര്‍വാര്‍ഡ് മെസേജുകള്‍ പാടില്ല. 

- ആശ്ലീല വീഡിയോയോ ചിത്രങ്ങളോ സംസാരങ്ങളോ പാടില്ല. 🔞 ( ഉടന്‍ തന്നെ ബാന്‍ ചെയ്യപ്പെടും)

- ക്രാക്ക് ചെയ്ത പ്രോഗ്രാമുകള്‍ നിയമ വിരുദ്ധമായി കോപ്പി ചെയ്ത പ്രോഗ്രാമുകള്‍ എന്നവ ഷെയര്‍ ചെയ്യുന്നതും ചോദിക്കുന്നതും ഒഴിവാക്കുക.
ഗ്രുപ്പില്‍ ഷെയര്‍ ചെയ്യുന്ന എല്ലാ മറ്റു ഗ്രുപ്പ് ലിങ്കുകള്‍, off-topic ലിങ്കുകള്‍ എന്നിവ അഡിമിനുകളുമായി ബന്ധപെട്ടതിനു ശേഷം മാത്രം ചെയ്യുക. അല്ലാത്തവ സ്പമായി കാണൂന്നതാണ്. 

- ഗ്രൂപ്പിലെ അംഗങളോട് അനുവാദമില്ലാതെ പേർസണൽ മെസേജ് ചെയ്യരുതു
## 


