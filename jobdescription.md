# Rules for job description

- Malayalam 
- English

## 



## Things to keep in mind while posting a job description

Keep in mind the following requirements before posting a job description

- Provide essential contact info such as phone, email id, company website ⚠️ [Madatory]

- Keep it concise

- Please include the nature of job/role that you are trying to fill

- Provide Required Experience and mode of work(remote or location)

- For freelancing jds: techstack of the project, features or outcome you are expecting out of it

- Recruiting agencies posting jds should provide information on client company and more details about the role. 

- Try to answer any follow-up questions asked here, in  the group itself


⚠⚠SPAM Messages without inadequate details will be deleted without any warnings

